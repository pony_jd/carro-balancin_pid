  #include<Wire.h>

// Pines para los motores
   // son para el motor izquierdo
  const int pwma = 5;
  const int ain2 = 6;
  const int ain1 = 7;
  
  //activa el controlador
  const int stby = 8;
  
  // motor Derecho
  const int bin1 = 10;
  const int bin2 = 9;
  const int pwmb = 11;


// Para Giroscopio
const int MPU_addr=0x68;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
 
int minVal=265;
int maxVal=402;
 
double x;
double y;
double z;

// Variables PID
int Ts, maxOP, minOP;    // Ts tiempo de muestreo, maxOP, minOP acotacion de los valores de salida
double Kp, Ki, Kd, PV, mSP;
/*
int setPoint = 8;
int histeresis = 3;
int velocidad = 30;
*/

// variables internas del controlador
unsigned long TiempoActual, TiempoAnterior;
double TiempoTranscurrido;
double OP, OP_1;        // Salida del controlador, OP actual, OP_1 anterior
double error, error_1, error_2;
double a,b,c;          // constantes de la ecuacion de diferencias


 
void setup(){

  // Inicializacion para MPU
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.begin(9600);

  // Inicializacion para driver de motores
  pinMode (pwma, OUTPUT);
  pinMode (ain1, OUTPUT);
  pinMode (ain2, OUTPUT);
  pinMode (stby, OUTPUT);
  pinMode (bin1, OUTPUT);
  pinMode (bin2, OUTPUT);
  pinMode (pwmb, OUTPUT);
  digitalWrite (stby, 1);

  // Configuracion de variables PID
  mSP = 50; // Punto de consigna
  Ts = 100; // Tiempo de muestreo (milisegundos)
  minOP = 0; // Mínimo valor de OP
  maxOP = 100; // Máximo valor de OP

      // Inicializar ki y kd en cero para sintonizar
  Kp = 1.0;             
  Ki = 0.0;
  Kd = 0.0;

  // Calcular las constantes a,b y c
  a = (Kp + Ki*Ts + Kd/Ts);
  b = -(Kp + 2*Kd/Ts);
  c = (Kd/Ts);

  // valores iniciales de las variables
  error = 0.0;
  error_1 = 0.0;
  error_2 = 0.0;
  OP_1 = 0.0;
  TiempoAnterior = 0;

  Serial.println("Arranca Balancin PID");
  delay
}
void loop(){
  // Modificar set point con modulo bluetooth
  if(Serial.available() > 2){
    setPoint = Serial.parseInt();
  }

  // Lectura del acelerometro y determinacion de los grados
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);
  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
  int xAng = map(AcX,minVal,maxVal,-90,90);
  int yAng = map(AcY,minVal,maxVal,-90,90);
  int zAng = map(AcZ,minVal,maxVal,-90,90);
  
  x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
  y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
  z= RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);

  Serial.print("AngleY= ");
  Serial.println(y);
  

  // determinacion de las variables
  TiempoActual = millis();
  if (millis() - TiempoAnterior >= Ts){
    // Determina la misma escala de 0°-180°, pero cambia la direccion, pero aun sin darle potencia a los motores
    if(y > 179){
      y = map(y, 360, 180, 0, 180);
      reversa();
      analogWrite(pwma, 0);                         //Motor Izquierdo
      analogWrite(pwmb, 0);                         //Motor Derecho
    }

    PV = map(y, 0, 180, 0, 100);                    // PV a porcentaje

    error = mSP - PV;                              //determina el error actual

    OP = a*error + b*error_1 + c*error_2 + OP_1;   // calcula la salida

    if(OP > maxOP) OP = maxOP;                    // Mínimo y Máximo valor de la OP
    else if(OP < minOP) OP = minOP;

    OP_1 = OP;                                   // Guardamos la salida actual en la anterior
    error_2 = error_1;       //actualizar los errores anteriores
    error_1 = error;        
    TiempoAnterior = TiempoActual; // Guardamos el tiempo anterior

    OP = map(OP,0,100,0,255);      // PWM de 8 bits (255)
    analogWrite(PIN_OP, OP);      // Mandamos la salida al pin PWM
    
  }
  /*if(y < (setPoint - histeresis)){
    Serial.println("Reversa");
    reversa();
    analogWrite(pwma, velocidad);    //Motor Izquierdo
    analogWrite(pwmb, velocidad);    //Motor Derecho
  }

  else if(y > (setPoint + histeresis)){
    Serial.println("adelante");
    adelante();
    analogWrite(pwma, velocidad);    //Motor Izquierdo
    analogWrite(pwmb, velocidad);    //Motor Derecho
  }

  else if(y < (setPoint + histeresis) && y > (setPoint - histeresis)){
    Serial.println("Detenido");
    detenido();
    analogWrite(pwma, 0);    //Motor Izquierdo
    analogWrite(pwmb, 0);    //Motor Derecho
  }

  delay(100); */

}



  void adelante()
  {
    digitalWrite(ain2, 0);            //Motor izquierdo
    digitalWrite(ain1, 1);
    digitalWrite(bin1, 1);            //Motor derecho
    digitalWrite(bin2, 0);
    
  }

    void reversa()
  {
    digitalWrite(ain2, 1);            //Motor izquierdo hacia atras
    digitalWrite(ain1, 0);
    digitalWrite(bin1, 0);            //Motor derecho   hacia atras
    digitalWrite(bin2, 1);
    
  }
  
  void detenido()
  {
    digitalWrite(ain2, 0);            //Motor izquierdo detenido
    digitalWrite(ain1, 0);
    digitalWrite(bin1, 0);            //Motor derecho  detenido
    digitalWrite(bin2, 0);
    
  }